# README #

This repository contains a project which is a part of Master Thesis at Politecnico di Milano.

### What is this repository for? ###
The code contains algorithms to parse, register, filter and visualize point clouds from LiDAR.

It is mainly designed for [KITTI dataset](http://www.cvlibs.net/datasets/kitti/) but should be
easily configured for other datasets containing IMU and LiDAR data.

* Templated filters parser, reader and visualizer of point clouds
* Based on [PCL library](http://www.pointclouds.org)

### How do I get set up? ###

* The project can be built using [cmake](http://www.cmake.org/)
* It was configured for [eclipse cdt](http://eclipse.org/cdt/)
* Dependencies: 
* * [PCL (>1.2)](http://www.pointclouds.org) 
* * [Boost](http://www.boost.org/) 
* * [Eigen](http://eigen.tuxfamily.org/)

### Parser ###

Parser works by first analyzing the loading type: file, folder or dataset.

The file 'example.cfl' serve as guidelines

* File loads a single file, Folder loads a whole folder without registering the pointclouds,
Dataset loads and tries to register pointclouds using IMU data
* Then each Filter Section is used to load a viewport and apply filters in cascade order

### Reader ###

The reader is configured to read pointclouds with KITTI files organization

### Visualizer ###

The visualizer was designed to keep things simple, so for each added pointcloud 
a new viewport is created

### Contribution ###

* Repo owner: Postica Gheorghii  [ggpostica@gmail.com](mailto:ggpostica@gmail.com)