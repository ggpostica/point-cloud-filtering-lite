/*
 * pcl_filter_main.cpp
 *
 *  Created on: Apr 24, 2015
 *      Author: george
 */

#include <iostream>

#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/obj_io.h>
#include <pcl/console/parse.h>

#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/frustum_culling.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/filters/grid_minimum.h>
#include <pcl/filters/local_maximum.h>
#include <pcl/filters/bilateral.h>
#include <pcl/filters/median_filter.h>

#include "filters/ChainFilter.hpp"
#include "filters/ConfigParser.h"
#include "utility/CloudVisualizer.h"
#include "utility/kitti.h"

// --------------
// -----Main-----
// --------------
int
main (int argc, char** argv)
{
	// --------------------------------------
	// -----Parse Command Line Arguments-----
	// --------------------------------------
	if(argc < 2){
		return 0;
	}

	int filters_added(0);
	int arguments = argc - 1;	// Consider the last argument as filename
	std::string filepath(argv[argc - 1]);

	pcl::PointCloud<pcl::PointXYZI>::Ptr cloud_ptr;

	if(argc == 2){
		//PROFILER_INIT_TIMER(TOPIC_ALL_EXECUTION_TIME, "Without visualisation...");
		//PROFILER_START_TIMER(TOPIC_ALL_EXECUTION_TIME);
		ConfigParser parser(argv[1]);
		boost::shared_ptr<CloudVisualizer> viz(new CloudVisualizer(1500, 800, "Point Cloud Filtering"));
		parser.parseAndShow(viz);
		//viz->setBackgroundColor(0.05, 0.05, 0.1);

		//PROFILER_STOP_TIMER(TOPIC_ALL_EXECUTION_TIME);

		ISAVE_TO_FILE("results.txt", PROFILER_SHOW_STATISTICS + ISHOW_ARCHIVE);

		viz->render(50000, 50);
		viz->waitClose();
		//parser.parse<pcl::PointXYZI>();
	}
}
