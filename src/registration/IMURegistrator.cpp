/*
 * imu_registrator.hpp
 *
 *  Created on: May 13, 2015
 *      Author: george
 */

#include "IMURegistrator.h"

inline void IMURegistrator::addPointCloud(cloud_ptr points, int index) {
	PROFILER_START_TIMER(TIMER_REGISTRATION_PREPARATION);
	pcl::transformPointCloud(*(this->input_filter.reapplyFilters(points)), *points, this->imu[index].pose);
	*(this->cloud) += *(points);
	PROFILER_STOP_TIMER(TIMER_REGISTRATION_PREPARATION);
}
