/*
 * point_plane_registrator.h
 *
 *  Created on: May 12, 2015
 *      Author: george
 */

#ifndef REGISTRATION_POINTPLANEREGISTRATOR_H_
#define REGISTRATION_POINTPLANEREGISTRATOR_H_

#include <pcl/registration/icp.h>
#include <pcl/registration/gicp.h>
#include <pcl/registration/ndt.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/features/normal_3d.h>

#include "KittiRegistrator.h"

class PointPlaneRegistrator: public KittiRegistrator {
public:
	PointPlaneRegistrator() : KittiRegistrator() { _init();	}

	PointPlaneRegistrator(boost::shared_ptr<CloudVisualizer> viewer)
	: KittiRegistrator(viewer) { _init(); }

	void setMinTransfThreshold(double epsilon){
		if(epsilon > 0) { estimator.setTransformationEpsilon(epsilon); }
	}
	void setMaxIterations(unsigned int max_iters){
		if(max_iters > 0) { estimator.setMaximumIterations(max_iters); }
	}
	void setMaxCorrispondence(double corr){
		if(corr > 0) { estimator.setMaxCorrespondenceDistance (corr); }
	}
	void setEuclideanFitness(double fitness){
		if(fitness > 0) { estimator.setEuclideanFitnessEpsilon(fitness); }
	}

protected:
	typedef typename boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI> > cloud_ptr;
	double performRegistration(cloud_ptr source, cloud_ptr target, cloud_ptr aligned, Eigen::Matrix4f &transform);

private:
	pcl::NormalEstimation<pcl::PointXYZI, pcl::PointNormal> norm_est;
	pcl::IterativeClosestPointNonLinear<pcl::PointNormal, pcl::PointNormal> estimator;
	typedef pcl::registration::TransformationEstimationPointToPlaneLLS<pcl::PointNormal, pcl::PointNormal> PointToPlane;
	boost::shared_ptr<PointToPlane> point_to_plane;

	void _init(){
		//PROFILER_REGISTER_MODULE("Point-to-Plane Estimator");

		estimator.setMaxCorrespondenceDistance (REG_MAX_CORRESPONDENCE);
		estimator.setMaximumIterations(REG_MAX_ITERATIONS);
		estimator.setTransformationEpsilon(REG_TRANSFORMATION_EPSILON);
		//estimator.setEuclideanFitnessEpsilon(1);
		boost::shared_ptr<pcl::search::KdTree<pcl::PointXYZI> > tree;
		tree = boost::shared_ptr<pcl::search::KdTree<pcl::PointXYZI> >(new pcl::search::KdTree<pcl::PointXYZI> ());
		norm_est.setSearchMethod (tree);
		norm_est.setKSearch (30);

		point_to_plane = boost::shared_ptr<PointToPlane>(new PointToPlane);
		estimator.setTransformationEstimation(point_to_plane);
	}
};

#endif /* REGISTRATION_POINTPLANEREGISTRATOR_H_ */
