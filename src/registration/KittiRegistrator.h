/*
 * kitti_registrator.h
 *
 *  Created on: May 8, 2015
 *      Author: george
 */

#ifndef REGISTRATION_KITTIREGISTRATOR_H_
#define REGISTRATION_KITTIREGISTRATOR_H_

#include "../config.h"

#include <pcl/registration/icp.h>
#include <pcl/registration/gicp.h>
#include <pcl/registration/ndt.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/features/normal_3d.h>

#include "../filters/ChainFilter.hpp"
#include "../utility/mini_profiler.h"
#include "../utility/info_printer.h"
#include "../utility/CloudVisualizer.h"
#include "Registrator.h"

class KittiRegistrator : public Registrator{
public:
	typedef typename boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI> > cloud_ptr;

	KittiRegistrator() : Registrator(){
		init();
	}

	KittiRegistrator(boost::shared_ptr<CloudVisualizer> viewer)
	: Registrator(viewer){
		init();
	}

	virtual void addPointCloud(cloud_ptr points, int index);

	virtual void finalize();

	virtual ~KittiRegistrator();

	virtual void setRegistrationFilter(ChainFilter<pcl::PointXYZI> &filter){
		env_filter = filter;
	}
	virtual void setInputFilter(ChainFilter<pcl::PointXYZI> &filter){
		input_filter = filter;
	}
	virtual void setOutputFilter(ChainFilter<pcl::PointXYZI> &filter){
		output_filter = filter;
	}
	virtual void setMinTransfThreshold(double epsilon){ }
	virtual void setMaxIterations(unsigned int max_iters){ }
	virtual void setMaxCorrispondence(double corr){ }
	virtual void setEuclideanFitness(double fitness){ }

protected:
	//ADD_OBJECT_ID(TOPIC_REGISTRATION);

	ChainFilter<pcl::PointXYZI> input_filter;
	ChainFilter<pcl::PointXYZI> env_filter;
	ChainFilter<pcl::PointXYZI> output_filter;

	cloud_ptr env;
	cloud_ptr last_sampled;

	double overall_distance;
	double total_fitness;
	int iter_counter;

	virtual void calculatePoses();
	virtual void prepare_();

	virtual double performRegistration(cloud_ptr source, cloud_ptr target, cloud_ptr aligned, Eigen::Matrix4f &transform);

private:
	double best_fit;

	void init(){
		PROFILER_INIT_TIMER(TIMER_REGISTRATION_COMPUTATION, "Point cloud registration computation time");
		PROFILER_INIT_TIMER(TIMER_REGISTRATION_PREPARATION, "Point cloud registration preparation time");
		env = cloud_ptr(new pcl::PointCloud<pcl::PointXYZI>());
		last_sampled = cloud_ptr(new pcl::PointCloud<pcl::PointXYZI>());
		best_fit = 0.0;
		overall_distance = 0.0;
		total_fitness = 0.0;
		iter_counter = 0;
	}
};

#endif /* REGISTRATION_KITTIREGISTRATOR_H_ */
