/*
 * linear_registrator.h
 *
 *  Created on: May 13, 2015
 *      Author: george
 */

#ifndef REGISTRATION_LINEARREGISTRATOR_H_
#define REGISTRATION_LINEARREGISTRATOR_H_

#include "KittiRegistrator.h"

class LinearRegistrator : public KittiRegistrator {
public:
	enum Estimator{
		IterativeClosestPoint, GeneralizedIterativeClosestPoint, NormalDistributionTransform
	};

	void setMinTransfThreshold(double epsilon){
		if(epsilon > 0) { estimator->setTransformationEpsilon(epsilon); }
	}
	void setMaxIterations(unsigned int max_iters){
		if(max_iters > 0) { estimator->setMaximumIterations(max_iters); }
	}
	void setMaxCorrispondence(double corr){
		if(corr > 0) { estimator->setMaxCorrespondenceDistance (corr); }
	}
	void setEuclideanFitness(double fitness){
		if(fitness > 0) { estimator->setEuclideanFitnessEpsilon(fitness); }
	}

	LinearRegistrator(Estimator est): KittiRegistrator(){
		_preinit(est);
		_init(estimator);
	}

	LinearRegistrator(Estimator est, boost::shared_ptr<CloudVisualizer> viewer): KittiRegistrator(viewer){
		_preinit(est);
		_init(estimator);
	}

	LinearRegistrator(boost::shared_ptr<pcl::Registration<pcl::PointXYZI, pcl::PointXYZI> > estimator
			= boost::shared_ptr<pcl::Registration<pcl::PointXYZI, pcl::PointXYZI> >(new pcl::IterativeClosestPoint<pcl::PointXYZI, pcl::PointXYZI>()))
	: KittiRegistrator() {
		_init(estimator);
	}

	LinearRegistrator(boost::shared_ptr<CloudVisualizer> viewer,
			boost::shared_ptr<pcl::Registration<pcl::PointXYZI, pcl::PointXYZI> > estimator
			= boost::shared_ptr<pcl::Registration<pcl::PointXYZI, pcl::PointXYZI> >(new pcl::IterativeClosestPoint<pcl::PointXYZI, pcl::PointXYZI>()))
	: KittiRegistrator(viewer) { _init(estimator); }

protected:
	typedef typename boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI> > cloud_ptr;
	double performRegistration(cloud_ptr source, cloud_ptr target, cloud_ptr aligned, Eigen::Matrix4f &transform);

private:
	boost::shared_ptr<pcl::Registration<pcl::PointXYZI, pcl::PointXYZI> > estimator;

	void _preinit(Estimator est){
		switch(est){
		case GeneralizedIterativeClosestPoint:{
			estimator = boost::shared_ptr<pcl::Registration<pcl::PointXYZI, pcl::PointXYZI> >
			(new pcl::GeneralizedIterativeClosestPoint<pcl::PointXYZI, pcl::PointXYZI>());
			break;
		}
		case NormalDistributionTransform: {
			boost::shared_ptr<pcl::NormalDistributionsTransform<pcl::PointXYZI, pcl::PointXYZI> > ndt =
					boost::shared_ptr<pcl::NormalDistributionsTransform<pcl::PointXYZI, pcl::PointXYZI> >
			(new pcl::NormalDistributionsTransform<pcl::PointXYZI, pcl::PointXYZI>());
			ndt->setStepSize(REG_NDT_STEP_SIZE);
			ndt->setResolution(REG_NDT_RESOLUTION);
			estimator = ndt;
			break;
		}
		default:{
			estimator = boost::shared_ptr<pcl::Registration<pcl::PointXYZI, pcl::PointXYZI> >
			(new pcl::IterativeClosestPoint<pcl::PointXYZI, pcl::PointXYZI>());
			break;
		}
		}
	}

	void _init(boost::shared_ptr<pcl::Registration<pcl::PointXYZI, pcl::PointXYZI> > est){
		estimator = est;
		estimator->setMaxCorrespondenceDistance (REG_MAX_CORRESPONDENCE);
		estimator->setMaximumIterations(REG_MAX_ITERATIONS);
		estimator->setTransformationEpsilon(REG_TRANSFORMATION_EPSILON);
		//estimator.setEuclideanFitnessEpsilon(1);
	}
};

#endif /* REGISTRATION_LINEARREGISTRATOR_H_ */
