/*
 * imu_registrator.h
 *
 *  Created on: May 13, 2015
 *      Author: george
 */

#ifndef REGISTRATION_IMUREGISTRATOR_H_
#define REGISTRATION_IMUREGISTRATOR_H_

#include "KittiRegistrator.h"

class IMURegistrator: public KittiRegistrator {
public:
	typedef typename boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI> > cloud_ptr;
	IMURegistrator(): KittiRegistrator() { }

	IMURegistrator(boost::shared_ptr<CloudVisualizer> viewer)
		: KittiRegistrator(viewer) { }

	void addPointCloud(cloud_ptr points, int index);

	protected:
		void setupEstimator(){ }
};

#endif /* REGISTRATION_ICP_REGISTRATOR_H_ */
