/* 		/_________________________________________________________________________________\
  			\_________________________________________________________________________/
			   |     |    |   |  | ||                         || |  |   |    |     |
			   |     |    |   |  | ||                         || |  |   |    |     |
			   |     |    |   |  | ||                         || |  |   |    |     |
			   |     |    |   |  | ||                         || |  |   |    |     |
			   |     |    |   |  | ||                         || |  |   |    |     |
			   |     |    |   |  | ||      CLOUD RENDERER     || |  |   |    |     |
			   |     |    |   |  | ||  						  || |  |   |    |     |
			  /----------------------\                       /----------------------\
			 /________________________\                     /________________________\
			***************************************************************************
		***********************************************************************************
	*******************************************************************************************
*/

#ifndef UTILITY_CLOUD_VISUALIZER_H
#define UTILITY_CLOUD_VISUALIZER_H

#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/visualization/pcl_visualizer.h>

#include "info_printer.h"

#define VIS_MAX_VIEWPORTS 12

class CloudVisualizer
{
public:
    typedef pcl::PointCloud<pcl::PointXYZI> cloudXYZI_t;

    CloudVisualizer(int width = 1600, int height = 1000, const std::string &name="CloudVisualizer");
    ~CloudVisualizer();

    void render(unsigned int us_delay = 100000, unsigned int ms_spin_time = 100);
    void stop();
    void waitClose();

    //boost::thread runOnThread();
    void operator()();

    void renameViewport(unsigned int id, const std::string &name);
    void addDetail(unsigned int viewport_id, const std::string &detail);

    void setBackgroundColor(double red, double green, double blue);
    void setPointSize(double point_size);
    void setTextSize(int font_size);
    int getNrViewports();

    int addPointCloud(boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI> > cloud,
    		const std::string &viewportName="", const std::string &details="");

    bool updatePointCloud(unsigned int v_id, boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI> > cloud,
    		const std::string &viewportName="", const std::string &details="");

    boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI> > getCloudPtr(unsigned int viewport_id);

    void showIterationStep(boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI> > source,
    		boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI> > target,
			boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI> > all, unsigned int spin_time = 100);

    void showIterationStep(boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI> > source, unsigned int spin_time = 100);
    void showIterationStep(boost::shared_ptr<pcl::PointCloud<pcl::PointNormal> > source, unsigned int spin_time = 100);

private:
    double bg_color[3];
    double point_size;
    int text_size;
    int window_width;
    int window_height;

    bool running;
    bool demo_running;

    boost::mutex demo_mutex;

    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
    boost::thread worker;

    /********************************************************************
     * 					Viewport management related						*
     ********************************************************************/
    struct Viewport{
    	int id;
    	int points;
    	std::string pc_name;
    	std::string title;
    	std::string description;
    	std::vector<std::string> details;
    	boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI> > cloud;

    	Viewport();

    	Viewport(int id, boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI> > cloud,
    			const std::string &v_title="", const std::string &v_details=":", const std::string &c_name=""){

    		pc_name = "Cloud_" + boost::lexical_cast<std::string>(id) + v_title;
    		if(v_title.empty()){
    			title = "Viewport " + boost::lexical_cast<std::string>(id + 1);
    		}else{
    			title = v_title;
    		}
    		description = v_details;
    		this->id = id;
    		this->cloud = cloud;
    		points = this->cloud->points.size();
    	}

    	virtual void addToViewer(const boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer,
    			double b_color[], double p_size, int t_size);
    protected:
    	virtual void addCloudToViewer(const boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer);
    };

    int nr_viewports;
    typedef typename boost::shared_ptr<Viewport> view_ptr;
    view_ptr viewports[VIS_MAX_VIEWPORTS];

    void runOnThread(unsigned int us_delay, unsigned int spin_time);
    void runDemoOnThread(unsigned int spin_time);
};

#endif // UTILITY_CLOUD_VISUALIZER_H
