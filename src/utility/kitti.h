/*
 * kitti_utility.h
 *
 *  Created on: Apr 24, 2015
 *      Author: george
 */

#ifndef UTILITY_KITTI_H_
#define UTILITY_KITTI_H_

#include <boost/thread/thread.hpp>
#include <boost/filesystem.hpp>

#include <pcl/common/common_headers.h>
#include <iostream>
#include <fstream>

#include "../filters/ChainFilter.hpp"
#include "../registration/KittiRegistrator.h"
#include "mini_profiler.h"
#include "info_printer.h"

namespace kitti {
	boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI> > loadFromFile(const std::string &pathname);

	boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI> > loadFromFolder(const std::string &pathname,
																			int min = 0, int max = -2);

	boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI> > loadSynchronized(const std::string &folderpath,
			boost::shared_ptr<Registrator> registrator
			= boost::shared_ptr<Registrator>(new KittiRegistrator()), int min = 0, int max = -2);

	boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI> > loadSynchronized(const std::string &velodyne_folder,
			const std::string &imu_folder, const std::string &calibration_filepath,
			boost::shared_ptr<Registrator> registrator
			= boost::shared_ptr<Registrator>(new KittiRegistrator()), int min = 0, int max = -2);

	static void addPoints(std::fstream& input, boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI> > points);

	static void readIMU(const std::string& folderpath, Registrator& reg, int min = 0, int max = -2);

	static void readTimestamps(const std::string& filepath, Registrator& reg, int min = 0, int max = -2);

	static void readLidarTimestamps(const std::string& filepath, Registrator& reg, int min = 0, int max = -2);

	static void readCalibration(const std::string& filepath, Registrator& reg);
};

#endif /* UTILITY_KITTI_H_ */
